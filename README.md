A presentation made for the SerresTech community. 

It is a basic introduction to OpenId-Connect and Keycloak

There are some related repositories, with example code:

* [A small spring-boot example] (https://gitlab.com/nikos5/keycloak-spring-boot-demo)
* [A primitive example to showcase message exchange] (https://gitlab.com/nikos5/openid-connect-simple-demo)


![CC BY NC Logo](https://i.creativecommons.org/l/by-nc/4.0/88x31.png) This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/)